import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isCollapsed = true;
  slideChangeMessage = '';
  myInterval = 3000;
  slides = [
    {
      image: 'https://d2kektcjb0ajja.cloudfront.net/images/posts/feature_images/000/000/072/large-1466557422-feature.jpg',
      text: 'assets 1'
    },
    {
      image: 'https://img-www.tf-cdn.com/movie/2/maze-runner-the-death-cure-2018.jpeg',
      text: 'assets 2'
    },
    {
      image: 'https://wallup.net/wp-content/uploads/2018/03/17/540626-Star_Wars-Star_Wars_The_Force_Awakens-movie_poster-Film_posters-748x421.jpg',
      text: 'assets 3'
    }
  ];

  log(event: number) {
    this.slideChangeMessage = `Slide has been switched: ${event}`;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
